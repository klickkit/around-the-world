package bhouse.travellist_starterproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


public class MainActivity extends Activity {

  private Menu menu;
  private boolean isListView;

  //Recycler View
  private RecyclerView mRecyclerView;
  private StaggeredGridLayoutManager mStaggeredGridlayoutManager;

  // Setting up the Adapter for the Recycler View

  private TravelListAdapter mAdapter;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    // Initializing UI Widgets

    mRecyclerView = (RecyclerView) findViewById(R.id.list);
    mStaggeredGridlayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
    mRecyclerView.setLayoutManager(mStaggeredGridlayoutManager);

    mAdapter = new TravelListAdapter(this);
    mRecyclerView.setAdapter(mAdapter);
    isListView = true;
    TravelListAdapter.OnItemClickListener onItemClickListener = new
          TravelListAdapter.OnItemClickListener() {
              @Override
                  public void onItemClick(View view, int position) {
                      ImageView placeImage = (ImageView) view.findViewById(R.id.placeImage);
                      LinearLayout placeNameHolder = (LinearLayout)view.findViewById(R.id.placeNameHolder);
                      Pair<View, String> imagePair = Pair.create((View) placeImage, "tImage");
                      Pair<View, String> holderPair = Pair.create((View) placeNameHolder, "tNameHolder");

                      ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this,
                          imagePair, holderPair);

                      Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                      intent.putExtra(DetailActivity.EXTRA_PARAM_ID, position);
                      ActivityCompat.startActivity(MainActivity.this, intent, options.toBundle());
                  }

          };
    mAdapter.setOnClickListener(onItemClickListener);

  }




  private void setUpActionBar() {

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_main, menu);
    this.menu = menu;
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.action_toggle) {
      toggle();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void toggle() {
    MenuItem item = menu.findItem(R.id.action_toggle);
    if (isListView) {
      mStaggeredGridlayoutManager.setSpanCount(2);
      item.setIcon(R.drawable.ic_action_list);
      item.setTitle("Show as list");
      isListView = false;
    } else {
      mStaggeredGridlayoutManager.setSpanCount(1);
      item.setIcon(R.drawable.ic_action_grid);
      item.setTitle("Show as grid");
      isListView = true;
    }
  }
}
